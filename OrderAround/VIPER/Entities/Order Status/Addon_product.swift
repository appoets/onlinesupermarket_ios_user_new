


import Foundation
struct Addon_product : Codable {
	let id : Int?
	let addon_id : ValueWrapper?
	let product_id : ValueWrapper?
	let price : ValueWrapper?
	let addon : Addon?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case addon_id = "addon_id"
		case product_id = "product_id"
		case price = "price"
		case addon = "addon"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		addon_id = try values.decodeIfPresent(ValueWrapper.self, forKey: .addon_id)
		product_id = try values.decodeIfPresent(ValueWrapper.self, forKey: .product_id)
		price = try values.decodeIfPresent(ValueWrapper.self, forKey: .price)
		addon = try values.decodeIfPresent(Addon.self, forKey: .addon)
	}

}
