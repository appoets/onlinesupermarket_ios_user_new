/* 
Copyright (c) 2018 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Banners : Codable {
	let product_id : ValueWrapper?
	let url : String?
	let position : ValueWrapper?
	let status : String?
	let shopstatus : String?
	let shopopenstatus : String?
	let shop : Shops?
	let product : Product?

	enum CodingKeys: String, CodingKey {

		case product_id = "product_id"
		case url = "url"
		case position = "position"
		case status = "status"
		case shopstatus = "shopstatus"
		case shopopenstatus = "shopopenstatus"
		case shop = "shop"
		case product = "product"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		
//        if let proID = try? values.decodeIfPresent(String.self, forKey: .product_id) {
//            product_id = Int(proID ?? "0")
//        }else if let id = try? values.decodeIfPresent(Double.self, forKey: .product_id) {
//            product_id = Int(id ?? 0.0)
//        }else {
//
//            product_id = try values.decodeIfPresent(Int.self, forKey: .product_id)
//
//
//        }
        
        product_id = try values.decodeIfPresent(ValueWrapper.self, forKey: .product_id)
		url = try values.decodeIfPresent(String.self, forKey: .url)
		position = try values.decodeIfPresent(ValueWrapper.self, forKey: .position)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		shopstatus = try values.decodeIfPresent(String.self, forKey: .shopstatus)
		shopopenstatus = try values.decodeIfPresent(String.self, forKey: .shopopenstatus)
		shop = try values.decodeIfPresent(Shops.self, forKey: .shop)
		product = try values.decodeIfPresent(Product.self, forKey: .product)
	}


}

enum ValueWrapper: Codable {
    case stringValue(String)
    case intValue(Int)
    case doubleValue(Double)
    case boolValue(Bool)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let value = try? container.decode(String.self) {
            self = .stringValue(value)
            return
        }
        if let value = try? container.decode(Bool.self) {
            self = .boolValue(value)
            return
        }
        if let value = try? container.decode(Double.self) {
            self = .doubleValue(value)
            return
        }
        if let value = try? container.decode(Int.self) {
            self = .intValue(value)
            return
        }

        throw DecodingError.typeMismatch(ValueWrapper.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for ValueWrapper"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case let .stringValue(value):
            try container.encode(value)
        case let .boolValue(value):
            try container.encode(value)
        case let .intValue(value):
            try container.encode(value)
        case let .doubleValue(value):
            try container.encode(value)
        }
    }

    var rawValue: String {
        var result: String
        switch self {
        case let .stringValue(value):
            result = value
        case let .boolValue(value):
            result = String(value)
        case let .intValue(value):
            result = String(value)
        case let .doubleValue(value):
            result = String(value)
        }
        return result
    }

    var intValue: Int? {
        var result: Int?
        switch self {
        case let .stringValue(value):
            result = Int(value)
        case let .intValue(value):
            result = value
        case let .boolValue(value):
            result = value ? 1 : 0
        case let .doubleValue(value):
            result = Int(value)
        }
        return result
    }

    var boolValue: Bool? {
        var result: Bool?
        switch self {
        case let .stringValue(value):
            result = Bool(value)
        case let .boolValue(value):
            result = value
        case let .intValue(value):
            result = Bool(truncating: value as NSNumber)
        case let .doubleValue(value):
            result = Bool(truncating: value as NSNumber)
        }
        return result
    }
    
    var doubleValue: Double? {
        var result: Double?
        switch self {
        case let .stringValue(value):
            result = Double(value)
        case let .boolValue(value):
            result = 0
        case let .intValue(value):
            result = Double(truncating: value as NSNumber)
        case let .doubleValue(value):
            result = Double(truncating: value as NSNumber)
        }
        return result
    }
}

//enum ValueWrapper: Codable {
//  case int(Int)
//  case string(String)
//
//  init(from decoder: Decoder) throws {
//    let container = try decoder.singleValueContainer()
//    do {
//      self = try .int(container.decode(Int.self))
//    } catch DecodingError.typeMismatch {
//      do {
//        self = try .string(container.decode(String.self))
//      } catch DecodingError.typeMismatch {
//        throw DecodingError.typeMismatch(ValueWrapper.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Encoded payload not of an expected type"))
//      }
//    }
//  }
//
//  func encode(to encoder: Encoder) throws {
//    var container = encoder.singleValueContainer()
//    switch self {
//    case .int(let int):
//      try container.encode(int)
//    case .string(let string):
//      try container.encode(string)
//    }
//  }
//}
