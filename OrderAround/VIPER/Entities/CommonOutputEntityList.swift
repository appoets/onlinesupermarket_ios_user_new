//
//  CommonOutputEntityList.swift
//  Project
//
//  Created by CSS on 10/10/18.
//  Copyright © 2018 css. All rights reserved.
//

import Foundation


struct OTP: JSONSerializable {
    
    var message: String?
    var otp: Int?
    
}


struct UserProfileDetails : Codable,JSONSerializable {
    var id : Int?
    var name : String?
    var email : String?
    var phone : String?
    var avatar : String?
    var device_token : String?
    var device_id : String?
    var device_type : String?
    var login_by : String?
    var social_unique_id : String?
    var stripe_cust_id : String?
    var wallet_balance : ValueWrapper?
    var otp : String?
    var braintree_id : String?
    var currency : String?
    var payment_mode : [String]?
    var addresses : [UserAddressDetails]?
    var cart : [Items]?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
        case email = "email"
        case phone = "phone"
        case avatar = "avatar"
        case device_token = "device_token"
        case device_id = "device_id"
        case device_type = "device_type"
        case login_by = "login_by"
        case social_unique_id = "social_unique_id"
        case stripe_cust_id = "stripe_cust_id"
        case wallet_balance = "wallet_balance"
        case otp = "otp"
        case braintree_id = "braintree_id"
        case currency = "currency"
        case payment_mode = "payment_mode"
        case addresses = "addresses"
        case cart = "cart"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        avatar = try values.decodeIfPresent(String.self, forKey: .avatar)
        device_token = try values.decodeIfPresent(String.self, forKey: .device_token)
        device_id = try values.decodeIfPresent(String.self, forKey: .device_id)
        device_type = try values.decodeIfPresent(String.self, forKey: .device_type)
        login_by = try values.decodeIfPresent(String.self, forKey: .login_by)
        social_unique_id = try values.decodeIfPresent(String.self, forKey: .social_unique_id)
        stripe_cust_id = try values.decodeIfPresent(String.self, forKey: .stripe_cust_id)
        wallet_balance = try values.decodeIfPresent(ValueWrapper.self, forKey: .wallet_balance)
        otp = try values.decodeIfPresent(String.self, forKey: .otp)
        braintree_id = try values.decodeIfPresent(String.self, forKey: .braintree_id)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        payment_mode = try values.decodeIfPresent([String].self, forKey: .payment_mode)
        addresses = try values.decodeIfPresent([UserAddressDetails].self, forKey: .addresses)
        cart = try values.decodeIfPresent([Items].self, forKey: .cart)
    }

}



struct UserProfile: JSONSerializable {

    var email: String?
    var id: Int?
    var login_by: LoginType?
    var name: String?
    var phone: String?
    var social_unique_id: String?
    var password: String?
    var password_confirmation: String?
    var accessToken: String?
    var device_id : String?
    var device_token : String?
    var device_type : DeviceType?

}

struct ForgotPasswordEntity : JSONSerializable {
    var message : String?
    var user : UserDataResponse?

}

//struct UserProfileDetails: JSONSerializable {
//
//    var email: String?
//    var id: Int?
//    var login_by: String?
//    var name: String?
//    var phone: String?
//    var social_unique_id: String?
//    var avatar: String?
//    var device_token: String?
//    var device_id: String?
//    var device_type: String?
//    var stripe_cust_id: String?
//    var wallet_balance: Int?
//    var otp: String?
//    var currency: String?
//    var payment_mode: [String]?
//    var addresses: [UserAddressDetails]?
//
//    var cart: [Items]?
//
//}

struct Message: JSONSerializable {
    var message : String?
}
//
